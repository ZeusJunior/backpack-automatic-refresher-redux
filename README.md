# Backpack Automatic Refresher Redux

### Before using this script, follow these instructions.

1. Make sure you have Python **3** installed. Get it here: https://www.python.org/downloads/
    - If you're on Windows, make sure the **"Add to PATH"** option is checked when installing Python **3**.
2. You need to get the steamid (ID64) of the inventory you want to refresh. You can find this on your backpacktf profile https://backpack.tf/u/765611... where the number at the end is the ID64
3. You need to get your user token. You can find this [here](https://backpack.tf/connections) 

---
### Running the script
- doubleclick the start script. `start.bat` for windows, `start.sh` for mac
- If this doesn't work:
    - On Windows, make sure **"Add to PATH"** was checked when installing Python **3**.
    - Your version of Python **3** must be up to date, uninstall any old version.

